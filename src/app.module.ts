import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AnimalsModule } from './animals/animals.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from './app.service';
import { AppController } from './app.controller';

@Module({
  controllers: [AppController],
  imports: [UsersModule,
            AnimalsModule,
            MongooseModule.forRoot('mongodb+srv://teste777:Wb7aMjDuYDQScQx@cluster0.uvhnl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')],
  providers: [AppService],
})
export class AppModule {}
