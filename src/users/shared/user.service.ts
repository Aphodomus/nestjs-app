import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../shared/user';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class UserService {
    constructor(@InjectModel('User') private readonly userModel: Model<User>) {

    }

    // Return all users on database
    async getAll() {
        try {
            return await this.userModel.find().exec();   
        } catch (error) {
            // If cant' returl all users from database
            throw new HttpException(`Can't return all users`, HttpStatus.NOT_FOUND);
        }
    }

    // Return a user by id on database
    async getById(id: string) {
        try {
            return await this.userModel.findById(id).exec();
        } catch (error) {
            // If the user doesn't exist in the database
            throw new HttpException(`User Id: ${id} not found`, HttpStatus.NOT_FOUND);
        }
    }

    // Create a new user on database
    async create(user: CreateUserDto) {
        try {
            const createUSer = new this.userModel(user);

            return await createUSer.save();
        } catch (error) {
            // If you can't create the user
            throw new HttpException(`Can't create the user with Id: ${user.id}`, HttpStatus.BAD_REQUEST);
        }
    }

    // Update a user on database
    async update(id: string, user: UpdateUserDto) {
        try {
            await this.userModel.updateOne({_id: id}, user).exec();

            return this.getById(id);
        } catch (error) {
            // If you can't update the user
            throw new HttpException(`Can't update the user with Id: ${id}`, HttpStatus.BAD_REQUEST);
        }
    }

    // Remove a user by id on batabase
    async remove(id: string) {
        try {
            return await this.userModel.deleteOne({_id: id}).exec();
        } catch (error) {
            // If you can't delete the user
            throw new HttpException(`Can't delete the user with Id: ${id}`, HttpStatus.BAD_REQUEST);
        }
    }
}
