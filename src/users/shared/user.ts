import { Document } from "mongoose";

export class User extends Document{
    email: string;
    password: string;
    name: string;
    age: number;
    salary: number;
    active: boolean;
}
