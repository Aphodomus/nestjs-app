import { IsBoolean, IsNumber, IsString } from "class-validator";
import { Document } from "mongoose";

export class CreateUserDto extends Document {
    @IsString()
    email: String;

    @IsString()
    password: String;

    @IsString()
    name: String;

    @IsNumber()
    age: Number;

    @IsNumber()
    salary: Number;

    @IsBoolean()
    active: Boolean;
}
