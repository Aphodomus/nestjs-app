import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { User } from './shared/user';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserService } from './shared/user.service';

@Controller('users')
export class UsersController {
    constructor(private userService: UserService) {

    }

    @Get()
    async getAll(): Promise<User[]> {
        return this.userService.getAll();
    }

    @Get(':id')
    async getById(@Param('id') id: string): Promise<User> {
        return this.userService.getById(id);
    }

    @Post()
    async create(@Body() user: CreateUserDto): Promise<CreateUserDto> {
        return this.userService.create(user);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() user: UpdateUserDto): Promise<UpdateUserDto> {
        return this.userService.update(id, user);
    }

    @Delete(':id')
    async remove(@Param('id') id: string) {
        this.userService.remove(id);
    }
}
