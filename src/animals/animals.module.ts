import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AnimalsController } from './animals.controller';
import { AnimalSchema } from './schemas/animal.schema';
import { AnimalService } from './shared/animal.service';

@Module({
    imports: [MongooseModule.forFeature([{name: 'Animal', schema: AnimalSchema}])],
    controllers: [AnimalsController],
    providers: [AnimalService]
})
export class AnimalsModule {}
