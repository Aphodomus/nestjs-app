import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Animal } from './animal'; 
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class AnimalService {
    constructor (@InjectModel('Animal') private readonly animalModel: Model<Animal>) {

    }

    // Return all animals from database
    async getAll() {
        try {
            return await this.animalModel.find().exec();
        } catch (error) {
            // If cant' returl all animals from database
            throw new HttpException(`Can't return all animals`, HttpStatus.NOT_FOUND);
        }
    }

    // Return a animal from database
    async getById(id: string) {
        try {
            return await this.animalModel.findById(id).exec();
        } catch (error) {
            // If the animal doesn't exist in the database
            throw new HttpException(`Animal Id: ${id} not found`, HttpStatus.NOT_FOUND);
        }
    }

    // Create a animal on database
    async create(animal: Animal) {
        try {
            const createAnimal = new this.animalModel(animal);

            return await createAnimal.save();
        } catch (error) {
            // If you can't create the animal
            throw new HttpException(`Can't create the animal with Id: ${animal.id}`, HttpStatus.BAD_REQUEST);
        }
    }

    // Update a animal on database
    async update(id: string, animal: Animal) {
        try {
            await this.animalModel.updateOne({_id: id}, animal).exec();

            return this.getById(id);   
        } catch (error) {
            // If you can't update the animal
            throw new HttpException(`Can't update the animal with Id: ${id}`, HttpStatus.BAD_REQUEST);
        }
    }

    // Remove a animal by id on database
    async remove(id: string) {
        try {
            return await this.animalModel.deleteOne({_id: id}).exec();   
        } catch (error) {
            // If you can't delete the animal
            throw new HttpException(`Can't delete the animal with Id: ${id}`, HttpStatus.BAD_REQUEST);
        }
    }
}
