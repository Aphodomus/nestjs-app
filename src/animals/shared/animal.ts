import { Document } from "mongoose";

export class Animal extends Document {
    kingdom: string;
    classification: string;
    color: string;
    size: number;
    weight: number;
    hair: boolean;
}
