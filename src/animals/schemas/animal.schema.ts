import * as mongoose from 'mongoose';

export const AnimalSchema = new mongoose.Schema({
    kingdom: String,
    classification: String,
    color: String,
    size: Number,
    weight: Number,
    hair: Boolean
})