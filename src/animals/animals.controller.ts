import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Animal } from './shared/animal';
import { AnimalService } from './shared/animal.service';

@Controller('animals')
export class AnimalsController {
    constructor(private animalService: AnimalService) {

    }

    @Get()
    async getAll(): Promise<Animal[]> {
        return this.animalService.getAll();
    }

    @Get(':id')
    async getById(@Param('id') id: string): Promise<Animal> {
        return this.animalService.getById(id);
    }

    @Post()
    async create(@Body() animal: Animal): Promise<Animal> {
        return this.animalService.create(animal);
    }
    
    @Put(':id')
    async update(@Param(':id') id: string, @Body() animal: Animal): Promise<Animal> {
        return this.animalService.update(id, animal);
    }

    @Delete(':id')
    async remove(@Param('id') id: string) {
        this.animalService.remove(id);
    }
}
